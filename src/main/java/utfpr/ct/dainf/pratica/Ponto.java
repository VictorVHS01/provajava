package utfpr.ct.dainf.pratica;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;

    public Ponto (){
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    
    public Ponto (double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public String toString() {
        return this.getNome() + "(" + x + ", " + y + ", " + z + ")";
    }

    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ponto other = (Ponto) obj;
        if(this.x == other.x)
            return true;
        if(this.y == other.y)
            return true;
        return other.z == this.z;
    }
    
    public double dist(Ponto ponto){
        
        return Math.sqrt(Math.pow(ponto.x - this.x, 2) + Math.pow(ponto.y - this.y, 2) + Math.pow(ponto.z - this.z, 2));
    }
    
    
    
    
    public String getNome() {
        return getClass().getSimpleName();
    }

}

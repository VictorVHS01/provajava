
import utfpr.ct.dainf.pratica.PontoXY;
import utfpr.ct.dainf.pratica.PontoXZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Pratica {

    public static void main(String[] args) {
        
        PontoXY xy = new PontoXY(0, 2);
        PontoXZ xz = new PontoXZ(-3, 2);

        System.out.println("Distancia = " + xy.dist(xz));
    }
    
}


package utfpr.ct.dainf.pratica;

public class PontoYZ extends Ponto2D{

    public PontoYZ() {
        super();
    }
    
    
    public PontoYZ(double y, double z){ 
        super.setX(0);
        super.setY(y);
        super.setZ(z);
    };
    
    @Override
    public String toString() {
        return this.getNome() + "(" + this.getX() + ", " + this.getY() + ", " + this.getZ() + ")";
    }
    
    
}

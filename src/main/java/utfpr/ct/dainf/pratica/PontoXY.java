
package utfpr.ct.dainf.pratica;

public class PontoXY extends Ponto2D{

    public PontoXY() {
        super();
    }
    
    
    public PontoXY(double x, double y){ 
        super.setX(x);
        super.setY(y);
        super.setZ(0);
    };
    
    @Override
    public String toString() {
        return this.getNome() + "(" + this.getX() + ", " + this.getY() + ", " + this.getZ() + ")";
    }
    
    
}

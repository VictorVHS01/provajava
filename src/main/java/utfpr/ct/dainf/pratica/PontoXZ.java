
package utfpr.ct.dainf.pratica;

public class PontoXZ extends Ponto2D{

    public PontoXZ() {
        super();
    }
    
    
    public PontoXZ(double x, double z){ 
        super.setX(x);
        super.setY(0);
        super.setZ(z);
    };
    
    @Override
    public String toString() {
        return this.getNome() + "(" + this.getX() + ", " + this.getY() + ", " + this.getZ() + ")";
    }
    
    
}
